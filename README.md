# dlpluck

A python script that moves files from child folders to a root download folder.

This script looks for files with the following file extensions:

* .avi
* .mov
* .mkv
* .mp4
* .mpeg
* .mpg

## Usage

This script has been tested with Python 3.7. It may work with older versions of Python but Python 3 is recommended.

To run, type:

`python dlpluck.py [relative filepath from location of script]`

For example:

`python dlpluck.py .`