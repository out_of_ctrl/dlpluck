import argparse
import os

# Define command line args
parser = argparse.ArgumentParser()
parser.add_argument("path", help="The path of the folder you want to scan")

# Initialise data structures
folders = []
filesfound = dict()
#movieFileTypes = ['.avi', '.mpeg', '.mpg', '.mkv', '.mp4']
movieFileTypes = ['.py', '.md']

def buildFiles(folderPath):
# Builds a dictionary of files from the folderPath folder and its subfolders

    filesfound.clear()

    for root, dirs, files in os.walk(folderPath):
        for d in dirs:
            for r, directory, f in os.walk('.'):
                for filer in f:
                    if any (x in filer for x in movieFileTypes):
                        if os.path.join(r, filer) != './'+filer:
                            filesfound[filer] = r 
                            if not r in folders:
                                folders.append(r)

def printFilenames():

    for key in filesfound:
       print (f"File: {key} | Path: {filesfound[key]}") 

def fileExtensionMapper(shortName):

    # Returns the file extension given a two char abbreviation

    if (shortName == "eg"):
        fileExtension = ".mpeg"
    
    if (shortName == "kv"):
        fileExtension = ".mkv"

    if (shortName == "p4"):
        fileExtension = ".mp4"
    
    if (shortName == "vi"):
        fileExtension = ".avi"
    
    if (shortName == "pg"):
        fileExtension == ".mpg"
    
    try: 
        return fileExtension
    
    except:
        raise Exception("No movie files found")

def moveFile():

    for key in filesfound:

        # Find out what is the file extension
        # Loop through list of file names and find out which matches

        relativePath = filesfound[key] + "/" + key
        fileType = fileExtensionMapper(key[-2:])
        newFilename = filesfound[key] + fileType

        print(f"Moving {relativePath} to {newFilename}")

        print(f"File extension {fileType}")

        #os.rename(relativePath, newFilename)

def deleteEmptyDirectories():

    while (len(folders) > 0):

        childFolder = folders.pop()

        print(f"Deleting the folder {childFolder}")

        #os.rmdir(childFolder)

if __name__ == "__main__":

    # Parse command line arguments
    args = parser.parse_args() 
    
    buildFiles(args.path)
    moveFile()
    deleteEmptyDirectories()